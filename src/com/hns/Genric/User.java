package com.hns.Genric;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hns.Utilities.Email;
import com.hns.Utilities.ReadProperties;

import junit.framework.Assert;

public class User {
	WebDriver driver = null;
	WebElement element = null;
	ReadProperties prop = new ReadProperties();
	Email email = new Email();

	public void login(WebDriver driver, String UserName, String Password) throws Exception {
		
		element = driver.findElement(By.id(prop.getProperty("LoginPage.emailUsername")));
		element.clear();
		element.sendKeys(UserName);
		element = driver.findElement(By.id(prop.getProperty("LoginPage.emailPassword")));
		element.clear();
		element = driver.findElement(By.id(prop.getProperty("LoginPage.emailPassword")));
		element.sendKeys(Password);
		element = driver.findElement(By.xpath(prop.getProperty("LoginPage.submitLogin")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("HomePage.userWardrode")));
		Assert.assertFalse(element.isDisplayed());
		element = driver.findElement(By.xpath(prop.getProperty("HomePage.userWishlist")));
		Assert.assertFalse(element.isDisplayed());
		element = driver.findElement(By.xpath(prop.getProperty("HomePage.userBag")));
		Assert.assertFalse(element.isDisplayed());
		
	}

	public void logout(WebDriver driver) throws Exception {
		
		element = driver.findElement(By.xpath(prop.getProperty("HomePage.userProfileMenu")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("HomePage.homeLogout")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("HomePage.homeLogin")));
		Assert.assertTrue(element.isDisplayed());
		
	}
	
	public void signUpUsingTwitter(WebDriver driver, Map<String, String> data) throws Exception {
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.singUpTwitter")));
		element.click();
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.twitterUsername")));
		element.clear();
		element.sendKeys(data.get("TwitterUsername"));
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.twitterPassword")));
		element.clear();
		element.sendKeys(data.get("TwitterPassword"));
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.twitterLogin")));
		element.click();
		
		
		if(driver.getCurrentUrl().contains("https://api.twitter.com/oauth/")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath(prop.getProperty("Register.twitterLogin")))));

			element = driver.findElement(By.xpath(prop.getProperty("Register.twitterLogin")));
			element.click();
		}
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.emailOther")));
		element.sendKeys(data.get("UserName"));
		element = driver.findElement(By.xpath(prop.getProperty("Register.createAccountPopup")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("Register.statusMessage")));
		String message = element.getText();
		Assert.assertEquals("A welcome message with further instructions has been sent to your e-mail address.",
				message);
		String signUpLink = email.getSignUpLink(data.get("EmailId"), data.get("EmailPassword"));
		driver.get(signUpLink);
		element = driver.findElement(By.xpath(prop.getProperty("Register.signUpLogin")));
		element.click();
		
		singUpSelectTitle(driver, data);
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.fullName")));
		String fullName = element.getAttribute("value");
		Assert.assertEquals(data.get("FullName"), fullName);
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.emailAddress")));
		String emailAddress = element.getAttribute("value");
		Assert.assertEquals(data.get("EmailId"), emailAddress);
		Assert.assertFalse(element.isEnabled());
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.userName")));
		element.clear();
		element.sendKeys(data.get("UserId"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.password")));
		element.clear();
		element.sendKeys(data.get("Password"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.confirmPassword")));
		element.clear();
		element.sendKeys(data.get("ConfrimPassword"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.letsGetStarted")));
		element.click();
		
		signUpSelectOptions(driver, data);
		signUpFinalize(driver, data);
		
	}
	
public void signUpUsingGoogle(WebDriver driver, Map<String, String> data) throws Exception {
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.signUpGoogle")));
		element.click();
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.gmailUsername")));
		element.clear();
		element.sendKeys(data.get("GoogleUsername"));
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.gmailNext")));
		element.click();
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.gmailPassword")));
		element.clear();
		element.sendKeys(data.get("GooglePassword"));
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.gmailSignIn")));
		element.click();
		
		if (driver.getCurrentUrl().contains("https://accounts.google.com/o/oauth2/auth?")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath(prop.getProperty("Register.gmailAllow")))));

			driver.findElement(By.xpath(prop.getProperty("Register.gmailAllow"))).click();
		}
		
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.emailOther")));
		element.sendKeys(data.get("UserName"));
		element = driver.findElement(By.xpath(prop.getProperty("Register.createAccountPopup")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("Register.statusMessage")));
		String message = element.getText();
		Assert.assertEquals("A welcome message with further instructions has been sent to your e-mail address.",
				message);
		String signUpLink = email.getSignUpLink(data.get("EmailId"), data.get("EmailPassword"));
		driver.get(signUpLink);
		element = driver.findElement(By.xpath(prop.getProperty("Register.signUpLogin")));
		element.click();
		
		singUpSelectTitle(driver, data);
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.fullName")));
		String fullName = element.getAttribute("value");
		Assert.assertEquals(data.get("FullName"), fullName);
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.emailAddress")));
		String emailAddress = element.getAttribute("value");
		Assert.assertEquals(data.get("EmailId"), emailAddress);
		Assert.assertFalse(element.isEnabled());
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.userName")));
		element.clear();
		element.sendKeys(data.get("UserId"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.password")));
		element.clear();
		element.sendKeys(data.get("Password"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.confirmPassword")));
		element.clear();
		element.sendKeys(data.get("ConfrimPassword"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.letsGetStarted")));
		element.click();
		
		signUpSelectOptions(driver, data);
		signUpFinalize(driver, data);
		
	}

	public void signUpUsingFacebook(WebDriver driver, Map<String, String> data) throws Exception {
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.singUpFacebook")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("Register.facebookEmailId")));
		element.clear();
		element.sendKeys(data.get("FacebookID"));
		element = driver.findElement(By.xpath(prop.getProperty("Register.facebookPassword")));
		element.clear();
		element.sendKeys(data.get("FacebookPassword"));
		element = driver.findElement(By.xpath(prop.getProperty("Register.facebookSubmit")));
		element.click();
		
		if(driver.getCurrentUrl().contains("https://www.facebook.com")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath(prop.getProperty("Register.facebookOkay")))));
			
			element = driver.findElement(By.xpath(prop.getProperty("Register.facebookOkay")));
			element.click();
		
		}
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.emailOther")));
		element.sendKeys(data.get("UserName"));
		element = driver.findElement(By.xpath(prop.getProperty("Register.createAccountPopup")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("Register.statusMessage")));
		String message = element.getText();
		Assert.assertEquals("A welcome message with further instructions has been sent to your e-mail address.",
				message);
		String signUpLink = email.getSignUpLink(data.get("EmailId"), data.get("EmailPassword"));
		driver.get(signUpLink);
		element = driver.findElement(By.xpath(prop.getProperty("Register.signUpLogin")));
		element.click();
		
		singUpSelectTitle(driver, data);
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.fullName")));
		String fullName = element.getAttribute("value");
		Assert.assertEquals(data.get("FullName"), fullName);
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.emailAddress")));
		String emailAddress = element.getAttribute("value");
		Assert.assertEquals(data.get("EmailId"), emailAddress);
		Assert.assertFalse(element.isEnabled());
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.userName")));
		element.clear();
		element.sendKeys(data.get("UserId"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.password")));
		element.clear();
		element.sendKeys(data.get("Password"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.confirmPassword")));
		element.clear();
		element.sendKeys(data.get("ConfrimPassword"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.letsGetStarted")));
		element.click();
		
		signUpSelectOptions(driver, data);
		signUpFinalize(driver, data);

	}
	
	
	private void singUpSelectTitle(WebDriver driver, Map<String, String> data) throws Exception {
		element = driver.findElement(By.xpath(prop.getProperty("Register.title")));
		element.click();

		switch (data.get("Title")) {
		case "Mr.":
			element = driver.findElement(By.xpath(prop.getProperty("Register.selectMr")));
			element.click();
			break;
		case "Mrs.":
			element = driver.findElement(By.xpath(prop.getProperty("Register.selectMrs")));
			element.click();
			break;
		case "Miss.":
			element = driver.findElement(By.xpath(prop.getProperty("Register.selectMiss")));
			element.click();
			break;
		}
	}
	
	public void signUpUsingMail(WebDriver driver, Map<String, String> data) throws Exception {

		element = driver.findElement(By.xpath(prop.getProperty("Register.signUpMail")));
		element.click();
		element = driver.findElement(By.className(prop.getProperty("Register.signUpFrame")));
		driver.switchTo().frame(element);
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.emailPopup")));
		element.sendKeys(data.get("UserName"));
		element = driver.findElement(By.xpath(prop.getProperty("Register.createAccountPopup")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("Register.statusMessage")));
		String message = element.getText();
		Assert.assertEquals("A welcome message with further instructions has been sent to your e-mail address.",
				message);
		String signUpLink = email.getSignUpLink(data.get("EmailId"), data.get("EmailPassword"));
		driver.get(signUpLink);
		element = driver.findElement(By.xpath(prop.getProperty("Register.signUpLogin")));
		element.click();

		element = driver.findElement(By.xpath(prop.getProperty("Register.profilePicture")));
		element.sendKeys(data.get("ProfilePicture"));

		singUpSelectTitle(driver, data);

		element = driver.findElement(By.xpath(prop.getProperty("Register.fullName")));
		element.clear();
		element.sendKeys(data.get("FullName"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.emailAddress")));
		String emailAddress = element.getAttribute("value");
		Assert.assertEquals(data.get("EmailId"), emailAddress);
		Assert.assertFalse(element.isEnabled());
		

		element = driver.findElement(By.xpath(prop.getProperty("Register.userName")));
		element.clear();
		element.sendKeys(data.get("UserId"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.password")));
		element.clear();
		element.sendKeys(data.get("Password"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.confirmPassword")));
		element.clear();
		element.sendKeys(data.get("ConfrimPassword"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.letsGetStarted")));
		element.click();

		signUpSelectOptions(driver, data);
		signUpFinalize(driver, data);

	}

	private void signUpSelectOptions(WebDriver driver, Map<String, String> data) throws Exception {

		for (int stepNo = 1; stepNo <= 5; stepNo++) {

			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(
					ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='step_" + stepNo + "']"))));

			element = driver.findElement(By.xpath("//div[@id='step_" + stepNo + "']"));
			List<WebElement> totalOptions = driver
					.findElements(By.xpath("//div[@id='edit-step-" + stepNo + "-term']/div[@class='taxonomy_markup']"));
			System.out.println("Total :" + totalOptions.size() + " Options for Step : " + stepNo);
			if (totalOptions.isEmpty()) {
				throw new Exception("Option not Found for Step: " + stepNo);
			}
			boolean optionFound = false;
			for (WebElement option : totalOptions) {
				System.out.println("Inside Options");
				element = option.findElement(By.xpath("div[2]/label"));
				String optionText = element.getText();
				System.out.println("Option : " + optionText);
				System.out
						.println("Excel Data of Step" + stepNo + "_Option : " + data.get("Step" + stepNo + "_Option"));
				if (data.get("Step" + stepNo + "_Option").equalsIgnoreCase(optionText)) {
					element = option.findElement(By.xpath("div"));
					element.click();
					optionFound = true;
					break;
				}
			}
			if (!optionFound) {
				throw new Exception(data.get("Step" + stepNo + "_Option") + " : option not found for Step - " + stepNo);
			}
		}
	}

	private void signUpFinalize(WebDriver driver, Map<String, String> data) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath(prop.getProperty("Register.finalFullName")))));
		
		element = driver.findElement(By.xpath(prop.getProperty("Register.finalFullName")));
		String fullName = element.getAttribute("value");
		Assert.assertEquals(data.get("FullName"), fullName);
		Assert.assertTrue(element.isEnabled());

		element = driver.findElement(By.xpath(prop.getProperty("Register.finalEmailID")));
		String emailId = element.getAttribute("value");
		Assert.assertEquals(data.get("EmailAddress"), emailId);
		Assert.assertFalse(element.isEnabled());

		element = driver.findElement(By.xpath(prop.getProperty("Register.finalUserName")));
		String UserId = element.getAttribute("value");
		Assert.assertEquals(data.get("UserId"), UserId);
		Assert.assertFalse(element.isEnabled());

		element = driver.findElement(By.xpath(prop.getProperty("Register.finalBio")));
		element.clear();
		element.sendKeys(data.get("Bio"));

		element = driver.findElement(By.xpath(prop.getProperty("Register.finalizeMyAccount")));
		element.click();
	}

	public void signUpVerifyDetails(WebDriver driver, Map<String, String> data) throws Exception {

		element = driver.findElement(By.xpath(prop.getProperty("HomePage.banner")));
		Assert.assertTrue(element.isDisplayed());

		element = driver.findElement(By.xpath(prop.getProperty("HomePage.userProfileMenu")));
		element.click();

		element = driver.findElement(By.xpath(prop.getProperty("HomePage.menuSettings")));
		element.click();

		element = driver.findElement(By.xpath(prop.getProperty("HomePage.settingsProfileTab")));
		element.click();

		element = driver.findElement(By.xpath(prop.getProperty("HomePage.settingsProfileTabFullName")));
		String settingsFullName = element.getAttribute("value");
		Assert.assertEquals(data.get("FullName"), settingsFullName);

		element = driver.findElement(By.xpath(prop.getProperty("HomePage.settingsProfileTabUsername")));
		String settingsUsername = element.getText();
		Assert.assertEquals(data.get("UserId"), settingsUsername);

		element = driver.findElement(By.xpath(prop.getProperty("HomePage.settingsProfileTabBio")));
		String settingsBio = element.getText();
		Assert.assertEquals(data.get("Bio"), settingsBio);
	}

}