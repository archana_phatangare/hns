package com.hns.Genric;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;


import com.hns.Utilities.ReadProperties;

public class Browser {
	
	ReadProperties rp = new ReadProperties();

	
	public WebDriver LaunchBrowser(String URL) throws Exception {
		
		WebDriver driver = null;
		
		String browser = rp.getProperty("GlobalConfig.browser");
		
		if(browser.equalsIgnoreCase("Firefox"))	{
			driver = new FirefoxDriver();
			driver.get(URL);
			driver.manage().window().maximize();
	
		}
		else if(browser.equalsIgnoreCase("Chrome"))
		{
			
			System.setProperty("webdriver.chrome.driver", "src/com/hns/Utilities/chromedriver");
			driver = new ChromeDriver();
			driver.get(URL);
		    driver.manage().window().maximize(); 	
		}
		return driver;
	}
	
	
}
