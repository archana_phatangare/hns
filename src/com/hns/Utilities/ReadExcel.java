package com.hns.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	public Map<String, String> byvalue(String filename, String value)
    {
		Map<String, String> exceldata = new HashMap<String, String>();
        try
        {
        	
            FileInputStream file = new FileInputStream(new File("src/com/hns/TestData/"+filename+".xlsx"));
 
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            XSSFSheet sheet = workbook.getSheetAt(0);
 
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
         
                Iterator<Cell> cellIterator = row.cellIterator();
                 
                	if(row.getCell(0).toString().toLowerCase().equals(value.toLowerCase()))
                	{
                		int cellcount=0;
                		 while (cellIterator.hasNext())
                         {	
                			 Row mainrow = sheet.getRow(0);
                			 Cell cell = cellIterator.next();
                			 cell.setCellType(Cell.CELL_TYPE_STRING);
                			 exceldata.put(mainrow.getCell(cellcount).toString(), cell.getStringCellValue());
                			 //System.out.println(mainrow.getCell(cellcount)+","+ cell.getStringCellValue());                			
                			 cellcount++;
                         }
                	}
                    
            }
            file.close();
          
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return exceldata;
    }
	
	
	
	public Map<String, String> readbyrow(String Filename,int rownumber)
	{
		Map<String, String> exceldata = new HashMap<String, String>();
        try
        {
        	
            FileInputStream file = new FileInputStream(new File("src/com/hns/TestData/"+Filename+".xlsx"));
 
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            XSSFSheet sheet = workbook.getSheetAt(0);
            
           Row row = sheet.getRow(rownumber);	            
         
                Iterator<Cell> cellIterator = row.cellIterator();
                 
                		int cellcount=0;
                		 while (cellIterator.hasNext())
                         {	
                			 Row mainrow = sheet.getRow(0);
                			 Cell cell = cellIterator.next();
                			 cell.setCellType(Cell.CELL_TYPE_STRING);
                			 exceldata.put(mainrow.getCell(cellcount).toString(), cell.getStringCellValue());
                			// System.out.println(mainrow.getCell(cellcount)+","+ cell.getStringCellValue());                			
                			 cellcount++;
                         }
                	                                  
            file.close();
          
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		return exceldata;
        
	}

}
