package com.hns.Testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Genric.Testcase;
import com.hns.Genric.User;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.ReadProperties;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS1_SignInMail {
	ReadProperties prop = new ReadProperties();
	Browser browser = new Browser();
	WebDriver driver = null;
	WebElement element = null;
	Screenshot screen = new Screenshot();
	String testcaseName = "HNS1_SignInMail";
	String screenshotName = null;
	Testcase testcase = new Testcase();
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Map<String, String> data = new HashMap<String, String>();

	@BeforeClass
	public void before() throws Exception {
		driver = browser.LaunchBrowser(prop.getProperty("GlobalConfig.url"));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath(prop.getProperty("HomePage.homeLogin"))).click();
	}

	@Test(priority=1)
	public void invalidUserName() throws Exception {
		try {
			data = excel.byvalue(testcaseName, "Scenario1");
			element = driver.findElement(By.id(prop.getProperty("LoginPage.emailUsername")));
			element.clear();
			element.sendKeys(data.get("UserName"));
			element = driver.findElement(By.id(prop.getProperty("LoginPage.emailPassword")));
			element.clear();
			element.sendKeys(data.get("Password"));
			element = driver.findElement(By.xpath(prop.getProperty("LoginPage.submitLogin")));
			element.click();
			element = driver.findElement(By.xpath(prop.getProperty("LoginPage.invalidCredential")));

			String errorMessage = element.getText();
			Assert.assertEquals("", errorMessage);
		} catch (Exception e) {
			if (driver != null)
				screenshotName = screen.capture(driver, testcaseName);

			testcase.fail(screenshotName);
			throw e;
		} catch (AssertionError error) {

			if (driver != null)
				screenshotName = screen.capture(driver, testcaseName);

			testcase.fail(screenshotName);
			throw error;
		}

	}

	@Test(priority=2)
	public void invalidPassword() throws Exception {

		try {
			data = excel.byvalue(testcaseName, "Scenario2");
			element = driver.findElement(By.id(prop.getProperty("LoginPage.emailUsername")));
			element.clear();
			element.sendKeys(data.get("UserName"));
			element = driver.findElement(By.id(prop.getProperty("LoginPage.emailPassword")));
			element.clear();
			element.sendKeys(data.get("Password"));
			element = driver.findElement(By.xpath(prop.getProperty("LoginPage.submitLogin")));
			element.click();
			element = driver.findElement(By.xpath(prop.getProperty("LoginPage.invalidCredential")));

			String errorMessage = element.getText();
			Assert.assertEquals("", errorMessage);
		} catch (Exception e) {

			if (driver != null)
				screenshotName = screen.capture(driver, testcaseName);

			testcase.fail(screenshotName);
			throw e;
		} catch (AssertionError error) {

			if (driver != null)
				screenshotName = screen.capture(driver, testcaseName);

			testcase.fail(screenshotName);
			throw error;
		}
	}

	@Test(priority=3)
	public void emptyFields() throws Exception {
		try {
			element = driver.findElement(By.id(prop.getProperty("LoginPage.emailUsername")));
			element.clear();
			element = driver.findElement(By.id(prop.getProperty("LoginPage.emailPassword")));
			element.clear();
			element = driver.findElement(By.xpath(prop.getProperty("LoginPage.submitLogin")));
			element.click();
			element = driver.findElement(By.xpath(prop.getProperty("LoginPage.invalidCredential")));

			String errorMessage = element.getText();
			Assert.assertEquals("", errorMessage);
		} catch (Exception error) {

			if (driver != null)
				screenshotName = screen.capture(driver, testcaseName);

			testcase.fail(screenshotName);
			throw error;
		} catch (AssertionError error) {

			if (driver != null)
				screenshotName = screen.capture(driver, testcaseName);

			testcase.fail(screenshotName);
			throw error;
		}
	}

	@Test(priority=4)
	public void validLogin() throws Exception {
		try {
			data = excel.byvalue(testcaseName, "Scenario4");
			user.login(driver, data.get("Username"), data.get("Password"));
			user.logout(driver);
		} catch (Exception e) {
			if (driver != null)
				screenshotName = screen.capture(driver, testcaseName);

			testcase.fail(screenshotName);
			throw e;
		} catch (AssertionError error) {

			if (driver != null)
				screenshotName = screen.capture(driver, testcaseName);

			testcase.fail(screenshotName);
			throw error;
		}
	}

	@AfterClass
	public void after() throws Exception {
		driver.quit();
	}
}
