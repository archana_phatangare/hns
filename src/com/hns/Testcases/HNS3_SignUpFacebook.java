package com.hns.Testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Genric.Testcase;
import com.hns.Genric.User;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.ReadProperties;
import com.hns.Utilities.Screenshot;

public class HNS3_SignUpFacebook {
	
	ReadProperties prop = new ReadProperties();
	Browser browser = new Browser();
	WebDriver driver = null;
	WebElement element = null;
	Screenshot screen = new Screenshot();
	String testCaseName = "HNS3_SignUpFacebook";
	String screenShotName = null;
	Testcase testcase = new Testcase();
	ReadExcel excel = new ReadExcel();
	Map<String, String> data = new HashMap<String, String>();
	User user = new User();
	
	@BeforeClass
	public void before() throws Exception {
		driver = browser.LaunchBrowser(prop.getProperty("GlobalConfig.url"));
	}

	@Test
	public void signUpUsingFacebook() throws Exception {
		try {
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			element = driver.findElement(By.xpath(prop.getProperty("HomePage.homeGetStarted")));
			element.click();
			
			user.signUpUsingFacebook(driver, data);
			user.signUpVerifyDetails(driver, data);


		} catch (Exception error) {
			if (driver != null)
				screenShotName = screen.capture(driver, testCaseName);

			testcase.fail(screenShotName);
			throw error;
		} catch (AssertionError error) {
			if (driver != null)
				screenShotName = screen.capture(driver, testCaseName);

			testcase.fail(screenShotName);
			throw error;
		}
	}

	@AfterClass
	public void after() throws Exception {
		driver.quit();
	}
}
